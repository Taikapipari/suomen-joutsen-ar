﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class Autofocus : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        CameraDevice.Instance.SetFocusMode(mode: CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            OnTouch();
        }
    }

    void OnTouch()
    {
 
        CameraDevice.Instance.SetFocusMode(mode: CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
    }

}
