﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public GameObject galleryPanel;
    public List<string> foundTrackables = new List<string>();
    //public GameObject[] galleryButtons;
    [SerializeField]
    List<Sprite> unlockedImages = new List<Sprite>();
    public Image[] galleryImages;
    public Button[] galleryButtons;
    public GameObject[] panels;
    public GameObject menuCanvas;



    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoad;
        galleryButtons = galleryPanel.GetComponentsInChildren<Button>();
    }

    public void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Menu")
        {
            menuCanvas.SetActive(true);
            UpdateUI(foundTrackables);
        }
        else
        {
            menuCanvas.SetActive(false);
        }
    }

    public void UpdateUI(List<string> trackablesFound)
    {
        foreach (var name in trackablesFound)
        {
            if (name == "Astronaut")
            {
                //galleryButtons[0].SetActive(true);
                galleryImages[0].GetComponent<Image>().sprite = unlockedImages[0];
                galleryButtons[0].GetComponent<TouchInteraction>().unlocked = true;
            }
            if (name == "Fissure")
            {
                //galleryButtons[1].SetActive(true);
                galleryImages[1].GetComponent<Image>().sprite = unlockedImages[1];
                galleryButtons[1].GetComponent<TouchInteraction>().unlocked = true;
            }
            if (name == "Drone")
            {
                //galleryButtons[2].SetActive(true);
                galleryImages[2].GetComponent<Image>().sprite = unlockedImages[2];
                galleryButtons[2].GetComponent<TouchInteraction>().unlocked = true;
            }
            if (name == "Oxygen")
            {
                galleryImages[3].GetComponent<Image>().sprite = unlockedImages[2];
                galleryButtons[3].GetComponent<TouchInteraction>().unlocked = true;
            }
        }
    }
}


    
