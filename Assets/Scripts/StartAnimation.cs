﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAnimation : MonoBehaviour
{
    Animator splashAnim; 
    // Start is called before the first frame update
    void Start()
    {
        splashAnim = gameObject.GetComponent<Animator>();
        CustomTrackingBehaviour.TrackableDetectedEvent += PlayAnimation;
    }


    void PlayAnimation()
    {
        splashAnim.SetTrigger("targetdetected");
    }
}
