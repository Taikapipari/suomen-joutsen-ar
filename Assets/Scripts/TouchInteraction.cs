﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchInteraction : MonoBehaviour
{
    public GameObject infoPanel;
    public bool unlocked = false;
    GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.instance;
    }

    public void OpenPanel()
    {
        if (unlocked == true)
        {
            foreach (var panel in gm.panels)
            {
                panel.gameObject.SetActive(false);
            }
            infoPanel.SetActive(true);
        }
    }


    // Update is called once per frame
    /*
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0); 
            if (touch.phase == TouchPhase.Ended && unlocked == true)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                if (Physics.Raycast(ray, out hit, 200))
                {
                    if (hit.transform.tag == "GALLERYIMAGE")
                    {
                        foreach (var panel in gm.panels)
                        {
                            panel.gameObject.SetActive(false);
                        }
                        
                    }

                }
            }
        }
    }
    */
}
