﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CustomTrackingBehaviour : DefaultTrackableEventHandler
{
    public delegate void TrackableDetected();
    public static event TrackableDetected TrackableDetectedEvent;
    public TextMeshProUGUI nameText;
    GameManager gm;
    string trackName;
    
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        gm = GameManager.instance;

    }

    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        nameText.gameObject.SetActive(true);
        trackName = mTrackableBehaviour.TrackableName;
        switch (trackName)
        {
            case "Astronaut":
                nameText.text = "Kapteenin pöytä";
                break;
            case "Fissure":
                nameText.text = "Kapteenin salkku";
                break;
            case "Drone":
                nameText.text = "Kapteenin kirjahylly";
                break;
            default:
                break;
        }
        if (!gm.foundTrackables.Contains(trackName))
        {
            gm.foundTrackables.Add(trackName);
            TrackableDetectedEvent?.Invoke(); 
        }
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();
        nameText.gameObject.SetActive(false);
    }
}
