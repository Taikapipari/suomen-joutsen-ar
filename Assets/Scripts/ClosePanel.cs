﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosePanel : MonoBehaviour
{

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            gameObject.SetActive(false);
        }
    }

}
