﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashTimer : MonoBehaviour
{
    public Image splashImage;
    public int splashDurationSecs;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadMenuTimed(splashDurationSecs));
    }


    IEnumerator LoadMenuTimed(int splashDuration)
    {
        StartCoroutine(FadeOut(splashDurationSecs));
        yield return new WaitForSecondsRealtime(splashDuration);
        SceneManager.LoadScene("menu");
    }

    IEnumerator FadeOut(int splashDuration)
    {
        for (float i = splashDuration; i >= 0; i -= Time.deltaTime)
        {
            splashImage.color = new Color(1, 1, 1, i);
            yield return null;
        }
    }
}
